#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T00:32:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CadastroGUI
TEMPLATE = app

INCLUDEPATH+=/usr/include/opencv
LIBS+=-L/usr/lib/x86_64-linux-gnu -lopencv_highgui -lopencv_core -lopencv_imgproc -lopencv_objdetect -lopencv_contrib

SOURCES += main.cpp\
        mainwindow.cpp \
    ../wrapper.cpp \
    cadastroaluno.cpp \
    reconhece.cpp \
    profcad.cpp \
    controlereserva.cpp

HEADERS  += mainwindow.h \
    ../wrapper.hpp \
    cadastroaluno.h \
    reconhece.h \
    profcad.h \
    controlereserva.h

FORMS    += mainwindow.ui \
    cadastroaluno.ui \
    reconhece.ui \
    profcad.ui \
    controlereserva.ui

QMAKE_EXTRA_TARGETS += pics
pics.commands= $(MKDIR)../Pics
