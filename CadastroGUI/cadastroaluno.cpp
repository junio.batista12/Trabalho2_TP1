/*  \file cadastroaluno.cpp
 *  \brief Método de cadastro
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "cadastroaluno.h"
#include "ui_cadastroaluno.h"
#include "mainwindow.h"
#include "QBoxLayout"
#include "fstream"
using namespace std;

CadastroAluno::CadastroAluno(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CadastroAluno)
{
    ui->setupUi(this);
    key=0;
}

CadastroAluno::~CadastroAluno()
{
    delete ui;
}

void CadastroAluno::run(){
    if (!frame.initCascade()){
        ui->Error->setText("Erro ao carregar arquivo XML");
    }
    while (char(key)!='q'){
        cam.getCapture();
        frame.setFrame(cam.getFrame());
        frame.detectFaces();
        frame.drawFaces(0,255,0);
        frame.showFrame();
        key = waitKey(10);
    }
}

void CadastroAluno::on_Cadastra_released()
{
    cam.free();
    key='q';
    if(frame.getFaces()==1){
        ofstream fout;
        string nome, matricula;
        nome=ui->Nome->text().toUtf8().constData();
        matricula=ui->Matricula->text().toUtf8().constData();
        frame.saveFaces(nome);
        fout.open("../Pics/data.csv",ios::app);
        fout<<nome<<';'<<matricula<<';'<<endl;
    }
    MainWindow *mainwindow=new MainWindow();
    mainwindow->show();
    this->close();
}
