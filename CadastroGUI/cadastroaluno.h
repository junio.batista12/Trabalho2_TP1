/*  \file cadastroaluno.hpp
 *  \brief Arquivo cabeçalho do método de implementação de cadastro de alunos
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#ifndef CADASTROALUNO_H
#define CADASTROALUNO_H

#include <QWidget>
#include "../wrapper.hpp"

namespace Ui {
class CadastroAluno;
}

class CadastroAluno : public QWidget
{
    Q_OBJECT

public:
    explicit CadastroAluno(QWidget *parent = 0);
    void run();
    ~CadastroAluno();

private slots:
    void on_Cadastra_released();

private:
    Ui::CadastroAluno *ui;
    Camera cam;
    FrameTreatment frame;
    int key;
};

#endif // CADASTROALUNO_H
