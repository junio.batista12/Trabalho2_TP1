/*  \file controlereserva.cpp
 *  \brief Método do controle de reserva
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "controlereserva.h"
#include "ui_controlereserva.h"

ControleReserva::ControleReserva(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControleReserva)
{
    ui->setupUi(this);
}

ControleReserva::~ControleReserva()
{
    delete ui;
}
