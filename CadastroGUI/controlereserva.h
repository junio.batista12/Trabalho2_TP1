/*  \file controlereserva.h
 *  \brief Arquivo cabeçalho do método de controle de reserva
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#ifndef CONTROLERESERVA_H
#define CONTROLERESERVA_H

#include <QWidget>

namespace Ui {
class ControleReserva;
}

class ControleReserva : public QWidget
{
    Q_OBJECT

public:
    explicit ControleReserva(QWidget *parent = 0);
    ~ControleReserva();

private:
    Ui::ControleReserva *ui;
};

#endif // CONTROLERESERVA_H
