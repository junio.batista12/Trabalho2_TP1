/*  \file main.cpp
 *  \brief Função Principal
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "mainwindow.h"
#include <QApplication>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
