/*  \file mainwindow.cpp
 *  \brief Métodos da interface gráfica da janela principal 
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cadastroaluno.h"
#include "reconhece.h"
#include "profcad.h"
//#include "reserva.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_AlunoButton_released()
{
    if(ui->CadButton->isChecked()){
        CadastroAluno *cadastro=new CadastroAluno;
        cadastro->show();
        this->close();
        cadastro->run();
    }
    else if(ui->RecButton->isChecked()){
        Reconhece *reconhece=new Reconhece;
        reconhece->show();
        reconhece->run();
        this->close();
    }
}

void MainWindow::on_ProfButton_released()
{
    if(ui->CadButton->isChecked()){
        ProfCad *cadastro = new ProfCad();
        cadastro->show();
        this->close();
        cadastro->run();
    }
    if(ui->ResButton->isChecked()){
    //    Reserva *reserva = new Reserva;
    //    reserva->show();
    //    this->close();
    }
}
