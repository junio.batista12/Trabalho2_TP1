/*  \file mainwindow.h 
 *  \brief Arquivo cabeçalho de métodos da interface gráfica da janela principal
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_AlunoButton_released();

    void on_ProfButton_released();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
