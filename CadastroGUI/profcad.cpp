/*  \file profcad.cpp
 *  \brief Métodos de cadastro do professor
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "profcad.h"
#include "ui_profcad.h"

ProfCad::ProfCad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProfCad)
{
    ui->setupUi(this);
    key=0;
}

ProfCad::~ProfCad()
{
    delete ui;
}
void ProfCad::run(){
    cam.getCapture();
    while(char(key)!='q'){
        frame.setFrame(cam.getFrame());
        frame.detectFaces();
        frame.drawFaces(0,0,255);
        frame.showFrame();
        key=waitKey(10);
    }
}
