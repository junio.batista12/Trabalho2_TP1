/*  \file profcad.h
 *  \brief Arquivo cabeçalho dos métodos de cadastro do professor 
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#ifndef PROFCAD_H
#define PROFCAD_H

#include <QWidget>
#include "../wrapper.hpp"

namespace Ui {
class ProfCad;
}

class ProfCad : public QWidget
{
    Q_OBJECT

public:
    explicit ProfCad(QWidget *parent = 0);
    ~ProfCad();
    void run();

private:
    Ui::ProfCad *ui;
    Camera cam;
    FrameTreatment frame;
    int key;
};

#endif // PROFCAD_H
