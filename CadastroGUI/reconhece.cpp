/*  \file reconhece.cpp
 *  \brief Métodos do reconhecimento de faces 
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#include "reconhece.h"
#include "ui_reconhece.h"
#include "QMessageBox"

Reconhece::Reconhece(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Reconhece)
{
    ui->setupUi(this);
}

Reconhece::~Reconhece()
{
    delete ui;    
}

void Reconhece::run(){
    while (char (key) != 'q'){
        cam.getCapture();
        frame.setFrame(cam.getFrame());
        frame.detectFaces();
        reconheceu = frame.recognize(nomePessoa);
        frame.drawFaces(0,255,0);
        frame.showFrame();
        if(reconheceu){
            cout<< "RECONHECEU voce " << nomePessoa << endl;
            //QMessageBox msgBox;
            //msgBox.setText(nomePessoa);
            //msgBox.exec();
        }
        key = waitKey(10);

    }
}


