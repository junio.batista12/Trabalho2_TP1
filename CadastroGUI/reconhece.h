/*  \file reconhece.h
 *  \brief Arquivo cabeçalho dos métodos de reconhecimento de face 
 *  \authors    Divino Júnio Batista Lopes
 *              Felipe Luis Rodrigues Sousa
 *              Eric do Vale de Castro
 */

#ifndef RECONHECE_H
#define RECONHECE_H

#include <QWidget>
#include "../wrapper.hpp"

namespace Ui {
class Reconhece;
}

class Reconhece : public QWidget
{
    Q_OBJECT

public:
    explicit Reconhece(QWidget *parent = 0);
    void run();
    ~Reconhece();

private:
    Ui::Reconhece *ui;
    string nomePessoa;
    FrameTreatment frame;
    bool reconheceu;
    Camera cam;
    int key;
};

#endif // RECONHECE_H
