# Segundo Trabalho de Técnicas de Programação 1
___

## Sistema de Controle de Acesso

Esse projeto consiste em um sistema de controle e monitoramento de acesso para ser utilizado na entrada dos Laboratórios de Informática (LINF - UnB).
Tem finalidade de monitorar alunos, professores, funcionários da limpeza, funcionários da segurança (vigias), pessoal do suporte técnico do CIC (funcionários, estagiários e professores ligados à administração do suporte) e também pessoas externas (ou de empresas tercerizadas), que acessam os laboratórios para dar manuntenção na infra-estrutura (hardware, cabeamento, móveis, paredes, ar-condicionados, etc.).



Os dados do cadastro serão armazenados em um Banco de Dados, o qual terá as seguintes informações:

>	Número de usuário, sendo matricula (para alunos) ou CPF (pessoal externo);
>	Nome e sobrenome completos;
>	Identificador de disciplinas ou turmas (para alunos) e outras reservas (pessoal externo);
>	Arquivos de imagens do rosto de cada usuário, que serão utilizados para validação de acesso;



No momento de validação de acesso, tem-se armazenamento das seguintes dados:

>	Autor da reserva;
>	Propósito da reserva e informações adicionais da reserva;
>	Número da sala;
>	Horário;
>	E se será uma reserva recorrente;


___
## Sistemas operacionais usados:

O projeto foi implementado em ambos Sistemas Operacionais Linux 16.04 e 17.04.


___
## Compiladores utilizados para execussão:

### Informações do terminal:

#### Ubuntu 17.04
>	compilador g++ (Ubuntu 6.3.0-12ubuntu2) 6.3.0 20170406
    	Copyright (C) 2016 Free Software Foundation, Inc.
    	This is free software; see the source for copying conditions.  There is NO warranty; not even for 		MERCHANTABILITY or FITNESS FOR        A PARTICULAR PURPOSE.


#### Ubuntu 16.04
>	g++ (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609
		Copyright (C) 2015 Free Software Foundation, Inc.
		This is free software; see the source for copying conditions.  There is NO
		warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

___
## Requisitos a serem instalados:

#### Instalando OpenCV - Versão 2.4.13.4:

As etapas de instalção estão desponíveis no link a seguir:

* https://gist.github.com/arthurbeggs/06df46af94af7f261513934e56103b30

Basta segui-las na ordem correta e estará com a biblioteca OpenCV disponível em sua máquina.



#### Instalando Doxygen:

Basta executar os seguintes comandos no seu terminal:

		$ sudo apt-get update
		$ sudo apt-get install doxygen-gui



#### Instalando Astah:

Basta baixar a versão _.deb_ no link:

* ((http://astah.net/com-announcement)[http://astah.net/com-announcement])

e executar o seguinte comandos no diretório em que foi salvo o seu _.deb_:

		$ sudo dpkg -i [nome_do_arquivo].deb

Como o _Astah_ tem como requisito _JDK_, será necessário a instalação do mesmo.



#### Instalando JDK:

Basta executar os seguintes comando em seu terminal:

		$ sudo add-apt-repository ppa:webupd8team/java
		$ sudo apt-get update
		$ sudo apt-get install oracle-java9-installer

___



## Método de compilação usado:

Primeiramente deve ser executado o arquivo _qmake_ que está disponível no diretório _CadastroGUI_, como a seguir:

		$ qmake

Com isso, será gerado um arquivo _Makefile_. Basta agora executar os seguintes comandos no seu terminal:

		$ make
		$ ./CadastroGUI

Para fazer a limpeza no diretório, basta executar o seguinte comando:

		$ make distclean


___



## Demonstrações da execução do projeto:

#### Executando identificador de faces:

![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Screenshots/Print1.jpg)



#### Executando a interface de cadastro:

>	Interface inicial de cadastro:

![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Screenshots/Print2.jpg)



>	Cadastrando Aluno:

![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Screenshots/Print3.png)



>	Cadastrando Professor:

![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Screenshots/Print4.jpg)


![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Screenshots/Print5.jpg)


___

## Documentação Doxygen:

A documentação Doxygen do backend se encontra disponível (na pasta html na raiz do git)[https://gitlab.com/junio.batista12/Trabalho2_TP1/tree/master/html] enquanto a parte gráfica se encontra disponível (na pasta html dentro de CadastroGUI)[https://gitlab.com/junio.batista12/Trabalho2_TP1/tree/master/CadastroGUI/html].
___



## Diagrama de Classes:


![alt text](https://gitlab.com/junio.batista12/Trabalho2_TP1/raw/master/Doxygen/Screenshot%20from%202017-11-28%2020-54-22.png)


Os diagramas de classes da interface estão adicionados no seguinte link: [diagramas de classe interface](https://gitlab.com/junio.batista12/Trabalho2_TP1/tree/master/CadastroGUI/html/Diagramas%20Interface).
___



## Diagrama de Sequência:

> Diagrama de sequência para cadastrar aluno (professor é análogo):

![diagramasequencia1](Screenshots/sequence diagram1.jpeg)

> Diagrama de sequência para reconhecimento:

![diagramadesequencia2](Screenshots/sequence diagram2.jpeg)
___








