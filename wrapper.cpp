/*  \file wrapper.cpp
 *  \brief Implementação dos métodos declarados em wrapper.h 
 *  \authors	Divino Júnio Batista Lopes
 *				Felipe Luis Rodrigues Sousa
 *				Eric do Vale de Castro
 */

#include "wrapper.hpp"

Camera::Camera(){
	capture=cvCaptureFromCAM(-1);
}

bool Camera::getCapture(){
	if(capture)
		return true;
	else
		return false;
}

Mat Camera::getFrame(){
	frame=cvQueryFrame(capture);
	flip(frame, frame, 1);
	return frame;
}

void Camera::free(){
    destroyAllWindows();
}

FrameTreatment::FrameTreatment(){
    face_cascade_name="../haarcascade_frontalface_alt.xml";
	windowname="Cadastro";
	fileNamePrefix="Testado";
	face_cascade.load(face_cascade_name);
}

bool FrameTreatment::initCascade(){
	if(!face_cascade.empty())
		return true;
	else
		return false;
}

void FrameTreatment::setFrame(Mat frame){
	this->frame=frame;
}

void FrameTreatment::showFrame(){
	imshow(windowname,frame);
}

void FrameTreatment::detectFaces(){
	Mat grayframe;
	cvtColor(frame,grayframe,CV_BGR2GRAY);
	equalizeHist(grayframe, grayframe);
    face_cascade.detectMultiScale(grayframe, faces, 1.1, 3, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
}

void FrameTreatment::drawFaces(int R, int G, int B){
    unsigned int i;
	for (i=0;i<faces.size();i++){
		rectangle(frame, faces[i], Scalar(B, G, R), 1, 8, 0);
	}
}

void FrameTreatment::saveFaces(string nome){
    Mat facetmp, frametmp;
    string filenametmp, finalnome="";
    int i;
	cvtColor(frame,frametmp,CV_BGR2GRAY);
    facetmp=frame(faces[0]);
    for (i=0;nome[i]!='\0';i++){
        if(nome[i]!=' '){
            finalnome+=nome[i];
        }
    }
    filenametmp="../Pics/"+finalnome+".jpg";
    imwrite(filenametmp,facetmp);
}

int FrameTreatment::getFaces(){
    return faces.size();
}

bool FrameTreatment::recognize(string nomePessoa){
    ifstream fin;
    vector<Mat> images;
    vector<int> labels;
    Mat tmp;
    string line, path="../Pics/";
    int i, j=0;
    fin.open("../Pics/data.csv");
    if(fin.is_open()){
        while(getline(fin, line)){
            for(i=0;line[i]!=';';i++){
                if(line[i] != ' '){
                    path+=line[i];
                }
            }
            tmp=imread(path,0);
            images.push_back(tmp);
            labels.push_back(j);
            j++;
        }

    Mat testSample = images[images.size() - 1];
    Ptr<FaceRecognizer> model = createLBPHFaceRecognizer();
    model->train(images, labels);
    int predictedLabel = model->predict(testSample);
    nomePessoa=path;


        if(predictedLabel > 70){
            return true;
        }
    }
    return false;
}

void Pessoa::setNome(string nomeAluno){

	nome = nomeAluno;

}

string Pessoa::getNome(){
	return nome;
}

void Pessoa::setTag(int id){
	tag = id;
}

int Pessoa::getTag(){
	return tag;
}

void Aluno::setMatricula(int idAluno){
	Matricula = idAluno;
}

int Aluno::getMatricula(){
	return Matricula;
}

void Professor::addAluno(Aluno aluno){
	
	
	estudante.push_back(aluno);
}
