/*! \file wrapper.hpp
 *  \brief Arquivo cabeçalho wrapper 
 *  \authors	Divino Júnio Batista Lopes
 *				Felipe Luis Rodrigues Sousa
 *				Eric do Vale de Castro
 */

#ifndef WRAPPER_H
#define WRAPPER_H

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "fstream"

#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace cv;

class Camera{
public:
	Camera();
	bool getCapture();
	Mat getFrame();
    void free();
private:
	CvCapture* capture;
	Mat frame;
};

class FrameTreatment{
public:
	FrameTreatment();
	bool initCascade();
	void setFrame(Mat);
	void showFrame();
	void detectFaces();
	void drawFaces(int,int,int);
    void saveFaces(string);
    int getFaces();
    bool recognize(string nomePessoa);
private:
	vector<Rect> faces;
	Mat frame;
	CascadeClassifier face_cascade;
	String windowname, face_cascade_name;
	string fileNamePrefix;
};

class Pessoa{
public:
    void setNome(string);
	void setTag(int);
	string getNome();
	int getTag();
private:
    string nome;
	int tag;
};

class Aluno: public Pessoa{
public:
	Aluno();
	void setMatricula(int);
	int getMatricula();
private:
	int Matricula;
};

class Professor: public Pessoa{
private:
	
	vector<Aluno> estudante;

public:
	Professor();
    void addAluno(Aluno aluno);
};

class Funcionario: public Pessoa{

};

class Reserva{
public:
    void setReserva(int);
    int getReserva();
private:
    int Reserva[6][6];
    vector<Aluno> alunos[6][6];
};


#endif
